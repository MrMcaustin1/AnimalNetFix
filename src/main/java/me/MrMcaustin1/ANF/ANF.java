package me.MrMcaustin1.ANF;

import com.google.common.collect.Lists;
import org.spongepowered.api.Game;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by MrMcaustin1 started on 1/11/2017 and finished on 1/12/2017
 * <p>
 * This plugin is to fix the Hatchery bug #23
 * </p>
 *
 * @author MrMcaustin1
 * @version v1.0.1-SNAPSHOT
 * @see <a href="https://github.com/GenDeathrow/Hatchery/issues/23">https://github.com/GenDeathrow/Hatchery/issues/23</a>
 */

@Plugin(id = "anf", name = "anf", version = "1.0.1")
public class ANF {

    private Game game = Sponge.getGame();
    private String animalNetID = "hatchery:animalnet";


    private ArrayList<String> anP = new ArrayList<>();

    @Listener
    public void onServerStart(GameInitializationEvent e) {

        //TODO: Add enabled msg

    }

    @Listener
    public void onPlayerInteract(InteractBlockEvent e, @First Player p) {
        if (!p.getItemInHand(HandTypes.MAIN_HAND).isPresent()) {
            return;
        }

        ItemStack stack = p.getItemInHand(HandTypes.MAIN_HAND).get();


        if (isItem(stack)) {
            //Debug test
//            Text itemName = Text.builder(stack.getItem().getName()).build();
//            p.sendMessage(itemName);

            //TODO: Give item in hand a UUID if it doesn't have one already
            String newitemUUID = itemUUID();


            if (stack.get(Keys.ITEM_LORE).isPresent()) {

                String itemUUID = stack.get(Keys.ITEM_LORE).get().toString().replace("Text{[", "").replace("}]", "");

//                Debug
//                Text msg = Text.builder(itemUUID).build();
//                p.sendMessage(msg);


                if (!anP.contains(itemUUID)) {

                    //Players first time clicking with Animal Net
                    anP.add(itemUUID);
                } else {

                    //Player releasing animal from net
                    giveAnimalNet(p);
                    anP.remove(itemUUID);
                }

            } else {
                p.setItemInHand(HandTypes.MAIN_HAND, setLore(stack, newitemUUID));
                anP.add(newitemUUID);

                Text msg1 = Text.builder("You cannot use another Animal Net until you clear this one!").color(TextColors.RED).build();
                p.sendMessage(msg1);
            }

        }
    }

    /**
     * Replaces the Animal Net containing the chicken with a regular animal net.
     *
     * @param p
     *         The player
     */
    public void giveAnimalNet(Player p) {


        //Animal Net itemstack
        ItemType modItemType = Sponge.getRegistry().getType(ItemType.class, animalNetID).get();
        ItemStack modItemStack = ItemStack.of(modItemType, 1);

        //Replaces the item in the hand with a blank Animal Net
        p.setItemInHand(HandTypes.MAIN_HAND, modItemStack);


    }

    /**
     * Compares the items
     *
     * @param stack
     *         The item you want to check
     *
     * @return True or False if the item is the item or not
     */

    public boolean isItem(ItemStack stack) {
        String comparedName = stack.getItem().getName();
        String chicken = "hatchery:animalnet";

        if (Sponge.getRegistry().getType(ItemType.class, animalNetID).isPresent()) {

            ItemType modItemType = Sponge.getRegistry().getType(ItemType.class, animalNetID).get();
            ItemStack modItemStack = ItemStack.of(modItemType, 1);


            return (stack.equalTo(modItemStack) || comparedName.equals(chicken));
        }
        return false;
    }

    /**
     * @return A string with a newly generated UUID
     */
    public String itemUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * @param stack
     *         The item stack which is getting a new lore
     * @param loreString
     *         The UUID or lore that is being set
     *
     * @return The new item stack
     */
    public ItemStack setLore(ItemStack stack, String loreString) {

        Text loreText = Text.builder(loreString).build();

        List<Text> lore = Lists.newArrayList(loreText);
        stack.offer(Keys.ITEM_LORE, lore);

        return stack;
    }
}
